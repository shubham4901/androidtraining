package com.example.androidpractice.helpers

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.androidpractice.Exercise1
import com.example.androidpractice.Exercise2
import com.example.androidpractice.R
import java.util.*
import kotlin.collections.ArrayList

class MainRecyclerAdapter() : RecyclerView.Adapter<MainRecyclerAdapter.ViewHolder>() {

    private var titles = ArrayList<Int>()
    private lateinit var context: Context

    constructor(context: Context) : this() {
        this.context = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.main_recyclerview_element, parent,false)
        return ViewHolder (v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.exercise.text = "Exercise" + titles[position]
        var intent: Intent? = null

        holder.exercise.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View?) {
                when (titles[position]) {
                    1 -> intent = Intent(context, Exercise1::class.java)
                    2 -> intent = Intent(context, Exercise2::class.java)
                    3 -> intent = Intent(context, Exercise1::class.java)
                    4 -> intent = Intent(context, Exercise1::class.java)
                    else -> {
                        Toast.makeText(context,"Exercise not found",Toast.LENGTH_SHORT)
                        return
                    }
                }
                context.startActivity(intent)
            }
        })
    }

    override fun getItemCount(): Int {
        return titles.size
    }

    fun setLists(titles: ArrayList<Int>) {
        this.titles = titles
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var exercise: TextView = itemView.findViewById(R.id.titleTV)

    }
}
