package com.example.androidpractice

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.androidpractice.helpers.MainRecyclerAdapter
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var adapter = MainRecyclerAdapter(this)
        var layoutManager = LinearLayoutManager(this)

        var mainRecyclerView = findViewById<RecyclerView>(R.id.mainRecyclerView)


        var titles: ArrayList<Int> = ArrayList()

        titles.add(1)
        titles.add(2)
        titles.add(3)
        titles.add(4)

        adapter.setLists(titles)

        mainRecyclerView.layoutManager = layoutManager
        mainRecyclerView.adapter = adapter

    }
}